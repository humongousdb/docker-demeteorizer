#!/bin/sh

mount_meteor="/meteor"
usage="docker run --rm -ti -v /path/to/meteor:/meteor:rw humongousdb/demeteorizer"

if [ ! -r $mount_meteor ]; then
  echo "! /meteor is not mounted. Usage: $usage"
  exit 1
elif [ ! -w $mount_meteor ]; then
  echo "! /meteor is mounted, but not writeable. Usage: $usage"
  exit 1
fi

check_code () {
  if [ ! "$1" -eq 0 ]; then
    echo "! Failure. Exiting..."
    exit 1
  fi
}

echo "] Copying Meteor application into local container filesystem with correct permissions..."

cp -r /meteor /local_meteor
check_code $?
chown -r root:root /local_meteor
check_code $?

meteor_path=/local_meteor

cd $meteor_path

echo "] Syncing Meteor..."

meteor --version
check_code $?

echo "] Running Demeteorizer on $meteor_path..."

rm -rf .demeteorized
check_code $?

demeteorizer .
check_code $?

echo "> Finished. Output in .demeteorizer."
exit 0
