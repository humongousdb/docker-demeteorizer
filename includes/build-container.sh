#!/bin/sh

check_code () {
  if [ ! "$1" -eq 0 ]; then
    echo "! Failure. Exiting..."
    exit 1
  fi
}

echo "] Installing latest version of Meteor..."

curl -0L http://install.meteor.com/ | sh
check_code $?

echo "] Installing latest version of Meteorite..."

npm install -g meteorite
check_code $?

echo "] Installing latest version of Demeteorizer..."

npm install -g demeteorizer
check_code $?

echo "Meteor version: `meteor --version`"
echo "Meteorite version: `mrt --version`"
echo "Demeteorizer version: `demeteorizer --version`"
