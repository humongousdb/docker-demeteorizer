docker-demeteorizer
===================

Start this image with your Meteor application mounted read-write at `/meteor`. It will be demeteorized in container context, output to `/meteor/.demeteorized`.

##Example

    $ docker run --rm -ti -v /path/to/meteor:/meteor:rw humongousdb/demeteorizer

##License

Please see the `LICENSE` file.
