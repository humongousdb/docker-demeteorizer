#!/bin/sh

DOCKER_IMAGE_STRING="humongousdb/demeteorizer"

echo "> Building $DOCKER_IMAGE_STRING..."
docker build -t $DOCKER_IMAGE_STRING "$@" .
