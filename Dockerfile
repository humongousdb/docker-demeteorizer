FROM    node:latest
COPY    includes/build-container.sh /build-container.sh
COPY    includes/demeteorize.sh /demeteorize.sh
RUN     chmod +x /build-container.sh /demeteorize.sh && \
        /build-container.sh
CMD     /demeteorize.sh
